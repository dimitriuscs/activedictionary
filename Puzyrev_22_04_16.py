
'''from flask import Flask, request, render_template, Markup
import codecs
from elasticsearch import Elasticsearch
es = Elasticsearch()

app = Flask(__name__)


@app.route('/')
def hello():
    start = u'''Добро пожаловать<br>
           на главную страницу Активного Словаря!<br>
           Вы можете начать поиск в меню.</p>'''
    return render_template('index.html', content=Markup(start))'''

#Dmitri Puzyrev
@app.route('/search', methods=['POST', 'GET'])
def search_res():
    word = request.form['search']
    res = es.search(index="dictionary", body={"query": {"match": {"word": word}}})
    arr = []
    if res['hits']['hits'] == []:
        arr.append('К сожалению, по вашему запросу ничего не найдено, попробуйте ещё раз.')
    heading = ''
    for hit in res['hits']['hits']:
        for a in hit['_source']:
            if a == 'word':
                arr.append('<b>' + hit['_source'][a].upper() + '</b>')
        for a in hit['_source']:
            if a == 'pos':
                heading += hit['_source'][a] + ';'
        for a in hit['_source']:
            if a == 'more':
                heading += hit['_source'][a]
    arr.append(heading)
    if heading != '':
        arr.append('')
    for hit in res['hits']['hits']:
        for a in hit['_source']:
            if a == 'subentry':
                for b in hit['_source']['subentry']:
                    for sub in b:
                        if sub == 'lexeme':
                            arr.append('<b>' + b[sub] + '</b>')
                    for sub in b:
                        if sub == 'examples':
                            arr.append('<i>' + b[sub] + '</i>')
                    for sub in b:
                        if sub == 'definition':
                            arr.append('<b>' + 'ЗНАЧЕНИЕ. ' + '</b>' + b[sub])
                    for sub in b:
                        if sub == 'comments':
                            arr.append('<b>' + 'КОММЕНТАРИИ.' + '</b>')
                            for com in b[sub]:
                                for text in com:
                                    arr.append(com[text])
                    for sub in b:
                        if sub == 'government':
                            arr.append('<b>' + 'УПРАВЛЕНИЕ.' + '</b>')
                            for act in b[sub]:
                                for text in act:
                                    arr.append(act[text])
                    for sub in b:
                        if sub == 'constructions':
                            arr.append('<b>' + 'КОНСТРУКЦИИ.' + '</b>')
                            arr.append('<i>' + b[sub] + '</i>')
                    for sub in b:
                        if sub == 'collocations':
                            arr.append('СОЧЕТАЕМОСТЬ. ' + '<i>' + b[sub] + '</i>')
                    for sub in b:
                        if sub == 'illustrations':
                            arr.append('<b>' + 'ИЛЛЮСТРАЦИИ.' + '</b>')
                            arr.append('<i>' + b[sub] + '</i>')
                    for sub in b:
                        if sub == 'synonyms':
                            arr.append('<b>' + 'СИН: ' + '</b>' + '<i>' + b[sub] + '</i>')
                    for sub in b:
                        if sub == 'analogs':
                            arr.append('<b>' + 'АНА: ' + '</b>' + '<i>' + b[sub] + '</i>')
                    for sub in b:
                        if sub == 'antonyms':
                            arr.append('<b>' + 'АНТ: ' + '</b>' + '<i>' + b[sub] + '</i>')
                    for sub in b:
                        if sub == 'conversives':
                            arr.append('<b>' + 'КОН: ' + '</b>' + '<i>' + b[sub] + '</i>')
                    for sub in b:
                        if sub == 'derivates':
                            arr.append('<b>' + 'ДЕР: ' + '</b>' + '<i>' + b[sub] + '</i>')
                    arr.append('')
    ending = ''
    for hit in res['hits']['hits']:
        for a in hit['_source']:
            if a == 'idioms':
                ending += '♢' + '<i>' + hit['_source'][a] + '</i>' + ' '
    for hit in res['hits']['hits']:
        for a in hit['_source']:
            if a == 'author':
                ending += hit['_source'][a]
    if ending != '':
        arr.append(ending)
    content = '<br>'.join([x for x in arr])
    return render_template('index.html', content=Markup(content))


'''@app.route('/zone')
def zone_search():
    return render_template('zonesearch.html')


@app.route('/zone_s', methods=['POST'])
def zone_res():
    word = request.form['word']
    zone = request.form['zone']
    print(word)
    print(zone)
    mas_answer = []
    if zone == "idioms":
        res = es.search(index="dictionary", body={"query": {"match": {"idioms": word}}})
        for hit in res['hits']['hits']:
            mas_answer.append(hit['_source']['idioms'])

    elif zone == "comments" or zone == "collocations" or zone == "constructions":
        res = es.search(index="dictionary", body={"query": {"match": {"subentry" + "." + zone + "." + str(zone[:3]) + "_text": word}}})
        print(res)
        d = {}
        for hit in res['hits']['hits']:
            for dicts in hit["_source"]["subentry"]:
                for key in dicts:
                    if key == zone:
                        for sub in dicts[key]:
                            for i in sub:
                                if word in sub[i]:
                                    d[dicts['lexeme']] = sub[i].replace(word, "</b>" + word + "</b>")
        for key in d:
            mas_answer.append("</b>" + key + "</b><br>" + d[key])

    else:
        res = es.search(index="dictionary", body={"query": {"match": {"subentry" + "." + zone: word}}})
        print(res)
        d = {}
        for hit in res['hits']['hits']:
            for dicts in hit["_source"]["subentry"]:
                for key in dicts:
                    if key == zone and word in dicts[key]:
                        print(dicts[key])
                        print(key)
                        d[dicts['lexeme']] = dicts[key].replace(word, "</b>" + word + "</b>")
        for key in d:
            mas_answer.append("</b>" + key + "</b><br>" + d[key])

    content = '<br><br>'.join([x for x in sorted(mas_answer)])

    return render_template('index.html', content=Markup(content))


if __name__ == '__main__':
    app.run(debug=True)'''


'''
json_data = dict()
json_data["id"] = 1
json_data["name"] = "Test"
keys_list = list("1", "2", "3")
json_data["keys"] = keys_list


name_data = dict()
name_data["first"] = "Test"
name_data["last"] = "Tester"
json_data["name"] = name_data
'''
