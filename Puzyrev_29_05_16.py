
from flask import Flask, request, render_template, Markup
import codecs
from elasticsearch import Elasticsearch
es = Elasticsearch()

app = Flask(__name__)


@app.route('/')
def hello():
    start = '''Добро пожаловать<br>
           на главную страницу Активного Словаря!<br>
           Вы можете начать поиск в меню.</p>'''
    return render_template('index.html', content=Markup(start))


@app.route('/search', methods=['POST', 'GET'])
def search_res():
    word = request.form['searchit']
    respreview = es.search(index="dict", body={"query": {"prefix": {"word": word}}})
    res = es.search(index="dict", body={"query": {"match": {"word": word}}}, size=1, analyzer="autocomplete")
    resfuzzy = es.search(index="dict", body={"query": {"fuzzy": {"word": word}}}, size=1, analyzer="autocomplete")
    arr = []
    heading = ''
    flag = False
    if res['hits']['hits'] == []:
        arr.append('<font color = FF0000> К сожалению, по вашему запросу ничего не найдено.')
        if resfuzzy['hits']['hits'] == []:
            flag = True
        else:
            arr.append('Возможно, вы имели в виду:</font>')
            res = resfuzzy
    if flag:
        arr.append('Статьи, начинающиеся на ' + word + ':</font>')
        for hit in respreview['hits']['hits']:
            arr.append('<b>' + hit['_source']['word'].lower() + '</b>')
    for hit in res['hits']['hits']:
        for a in hit['_source']:
            if a == 'word':
                arr.append('<b>' + hit['_source'][a].upper() + '</b>')
        for a in hit['_source']:
            if a == 'pos':
                heading += hit['_source'][a] + ';'
        for a in hit['_source']:
            if a == 'more':
                heading += hit['_source'][a]
    arr.append(heading)
    if heading != '':
        arr.append('')
    for hit in res['hits']['hits']:
        for a in hit['_source']:
            if a == 'subentry':
                for b in hit['_source']['subentry']:
                    for sub in b:
                        if sub == 'lexeme':
                            arr.append('<b>' + b[sub] + '</b>')
                    for sub in b:
                        if sub == 'examples':
                            arr.append('<i>' + b[sub] + '</i>')
                    for sub in b:
                        if sub == 'definition':
                            arr.append('<b>' + 'ЗНАЧЕНИЕ. ' + '</b>' + b[sub])
                    for sub in b:
                        if sub == 'constructions':
                            arr.append('<b>' + 'КОНСТРУКЦИИ.' + '</b>')
                            arr.append('<i>' + b[sub] + '</i>')
                    for sub in b:
                        if sub == 'collocations':
                            arr.append('СОЧЕТАЕМОСТЬ. ' + '<i>' + b[sub] + '</i>')
                    for sub in b:
                        if sub == 'illustrations':
                            arr.append('<b>' + 'ИЛЛЮСТРАЦИИ.' + '</b>')
                            arr.append('<i>' + b[sub] + '</i>')
                    for sub in b:
                        if sub == 'synonyms':
                            arr.append('<b>' + 'СИН: ' + '</b>' + '<i>' + b[sub] + '</i>')
                    for sub in b:
                        if sub == 'analogs':
                            arr.append('<b>' + 'АНА: ' + '</b>' + '<i>' + b[sub] + '</i>')
                    for sub in b:
                        if sub == 'antonyms':
                            arr.append('<b>' + 'АНТ: ' + '</b>' + '<i>' + b[sub] + '</i>')
                    for sub in b:
                        if sub == 'conversives':
                            arr.append('<b>' + 'КОН: ' + '</b>' + '<i>' + b[sub] + '</i>')
                    for sub in b:
                        if sub == 'derivates':
                            arr.append('<b>' + 'ДЕР: ' + '</b>' + '<i>' + b[sub] + '</i>')
                    arr.append('')
    ending = ''
    for hit in res['hits']['hits']:
        for a in hit['_source']:
            if a == 'idioms':
                ending += '♢' + '<i>' + hit['_source'][a] + '</i>' + ' '
    for hit in res['hits']['hits']:
        for a in hit['_source']:
            if a == 'author':
                ending += hit['_source'][a]
    if ending != '':
        arr.append(ending)
    content = '<br>'.join([x for x in arr])
    return render_template('index.html', content=Markup(content))


'''@app.route('/zone')
def zone_search():
    return render_template('zonesearch.html')


@app.route('/zone_s', methods=['POST'])
def zone_res():
    word = request.form['word']
    zone = request.form['zone']
    mas_answer = []
    lex_word = {}
    zones_rus = {"idioms" : "идиомы", "definition" : "значение",
                 "examples" : "примеры", "comments" : "комментарии",
                 "constructions" : "конструкции", "collocations" : "сочетаемость",
                 "illustrations" : "иллюстрации", "synonyms" : "синонимы",
                 "analogs" : "аналоги", "derivates" : "дериваты",
                 "antonyms" : "антонимы", "conversives" : "конверсивы"}
    if zone == "idioms":
        res = es.search(index="dictionary", body={"query": {"match": {"idioms": word}}})
        for hit in res['hits']['hits']:
            mas_answer.append(hit['_source']['idioms'].replace(word, "<b>" + word + "</b>"))
    else:
        res = es.search(index="dictionary", body={"query": {"match": {"subentry" + "." + zone: word}}})
        for hit in res['hits']['hits']:
            for dicts in hit["_source"]["subentry"]:
                for key in dicts:
                    if key == zone and word in dicts[key]:
                        lex_word[dicts['lexeme']] = dicts[key].replace(word, "<b>" + word + "</b>")
        for key in lex_word:
            mas_answer.append("<b>" + key + "</b><br>" + lex_word[key])
    size_of_l = len(mas_answer)
    if size_of_l > 10 and str(size_of_l)[-2] != "1":
        res_word = "результатов"
    elif str(size_of_l)[-1] == "1":
        res_word = "результат"
    elif str(size_of_l)[-1] in ["2", "3", "4"]:
        res_word = "результата"
    else:
        res_word = "результатов"
    content = "<h5>" +"Поиск "  + "<b>" + word + "</b>" + " в зоне "  +\
              "<b>" + str(zones_rus[zone]) +  "</b>" +" " + "<b>" + str(size_of_l) + "</b>" + \
              " " + res_word + "</h5>" + "<hr>" + '<br><br>'.join([x for x in sorted(mas_answer)])
    return render_template('index.html', content=Markup(content))


@app.route('/gov', methods=['POST'])
def gov_res():
    mas_answer = []
    d = {}
    temp = []
    if type == None:
        res = es.search(index="dict", body={"query": {"match": {"subentry.government.actant": actant}}})
        for hit in res['hits']['hits']:
                for dicts in hit["_source"]["subentry"]:
                    for key in dicts:
                        if key == "government":
                            for sub in dicts[key]:
                                if sub["actant"] == actant:
                                    temp.append("<b>" + sub['actant'] + "</b>" + " " + sub["case"] + " " + sub["example"])

                            d[dicts['lexeme']] = '\n'.join([x for x in temp])
                            temp = []
    elif actant == None:
        res = es.search(index="dictionary", body={"query": {"match": {"subentry.government.case": type}}})
        for hit in res['hits']['hits']:
                for dicts in hit["_source"]["subentry"]:
                    for key in dicts:
                        if key == "government":
                            for sub in dicts[key]:
                                if type in sub["case"]:
                                    temp.append(sub['actant'] + " " + "<b>" + sub["case"] + "</b>" + " " + sub["example"])
                            d[dicts['lexeme']] = '\n'.join([x for x in temp])
                            temp = []

    else:
        res = es.search(index="dictionary", body={"query": {"match": {"subentry.government.actant": actant}}})
        for hit in res['hits']['hits']:
                for dicts in hit["_source"]["subentry"]:
                    for key in dicts:
                        if key == "government":
                            for sub in dicts[key]:
                                if type in sub["case"] and sub["actant"] == actant:
                                    temp.append("<b>" + sub['actant'] + "</b>" + " " + "<b>" + sub["case"] + "</b>" + " " + sub["example"])
                            d[dicts['lexeme']] = '\n'.join([x for x in temp])
                            temp = []

    size_of_l = len(d)
    if size_of_l > 10 and str(size_of_l)[-2] != "1":
        res_word = "результатов"
    elif str(size_of_l)[-1] == "1":
        res_word = "результат"
    elif str(size_of_l)[-1] in ["2", "3", "4"]:
        res_word = "результата"
    for key in d:
        mas_answer.append("<b>" + key + "</b><br>" + d[key])

    if actant == None:
        content = "<h5>" + "Поиск с параметрами:"  + "<b>" + type + "</b>" +\
                  " " + "<b>" + str(size_of_l) + "</b>" + \
                  ": " + res_word + "</h5>" + "<hr>" + '<br><br>'.join([x for x in sorted(mas_answer)])
    elif type == None:
        content = "<h5>" + "Поиск с параметрами:"  + "<b>" + actant + "</b>" +\
                  ": " + "<b>" + str(size_of_l) + "</b>" + \
                  " " + res_word + "</h5>" + "<hr>" + '<br><br>'.join([x for x in sorted(mas_answer)])
    else:
        content = "<h5>" + "Поиск с параметрами:"  + "<b>" + actant + " " + type + "</b>" +\
                  ": " + "<b>" + str(size_of_l) + "</b>" + \
                  " " + res_word + "</h5>" + "<hr>" + '<br><br>'.join([x for x in sorted(mas_answer)])
    return render_template('index.html', content=Markup(content))

if __name__ == '__main__':
    app.run(debug=True)'''


'''
json_data = dict()
json_data["id"] = 1
json_data["name"] = "Test"
keys_list = list("1", "2", "3")
json_data["keys"] = keys_list


name_data = dict()
name_data["first"] = "Test"
name_data["last"] = "Tester"
json_data["name"] = name_data
'''
